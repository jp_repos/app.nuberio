<?php
class Mdl_main extends CI_Model {
    public function __construct() {
		$this -> load -> database();
	}

    function get_activities($user_id){
		$q="SELECT elements_id from `actividades` WHERE usuarios_id = {$user_id}";
		$x = $this->db->query($q);
		return ($x)?$x -> row_array() : false;
	}

    function get_menu_items($id){
		$q="SELECT * from `actividades` WHERE usuarios_id = {$id}";
		$x = $this->db->query($q);
		$act = ($x->result_id->num_rows)?$x->result_array() : false;
		$e=[];
		$res=[];
		$eitm=[];
		if(!empty($act)){
			foreach ($act as $elm) {
				$e[]=['element_id'=>$elm['elements_id'],'sub_elem_id'=>$elm['sub_elements_id']];

			}
			foreach ($e as $ex) {
				$i = $this->db->query("SELECT nombre,controller FROM visual_elements where id = {$ex['element_id']}")->row();
				$sbx_i = [];
				$sub_i = explode(',',$ex['sub_elem_id']);
				foreach ($sub_i as $sbx) {
					$sbx_i[] = $this->db->query("SELECT nombre,method FROM visual_sub_elements where id = {$sbx}")->row();
				}

				$eitm[] = [$i,$sbx_i];
			}
		}
		// if(!empty($e) && !epmty($sub_e)){
		//
		// 	$res[]
		// }
		return $eitm;
	}


}