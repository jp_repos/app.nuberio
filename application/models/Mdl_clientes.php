<?php
class mdl_clientes extends CI_Model {

	function __construct() {
		$this ->load->database();
    }
    
    function new_cliente(){
        $q = "SELECT label,value,title,(SELECT nombre FROM visual_objects WHERE id = vis_elem_type) as 'vis_elem_type',vis_ord_num,validates FROM `atoms_struct` WHERE atom_types_id = 1 AND vis_ord_num > 0  AND vis_ord_num < 99 ORDER BY vis_ord_num ASC ";
        $r = $this->db->query($q)->result_array();
        return (is_array($r))?$r:[];
    }   
   
    function save_new_cliente($p){
        $a = new Atom(0,'CLIENTE');
        foreach ($p['fields'] as $pcle) {
          $a->pcle_update($pcle['lavel'],$pcle['value']);
        }
        // return $a->id;
    }

    function check_for_duplicated($label,$value){
        $q = "SELECT id FROM atoms_pcles WHERE label = {$label} AND value = {$value} ";
        $r = $this->db->query($q)->result();
        return (is_object($r))?$r:false;
    }
    
    function new_service($id){
      $s = new Atom($id);
      $stype = ($s->get_pcle('recurrente')->value == "SI")?3:4;
      $q = "SELECT label,
               value,
               title,
               (SELECT nombre FROM visual_objects WHERE id = vis_elem_type) as 'vis_elem_type',vis_ord_num 
               FROM `elements_struct` WHERE elements_types_id = {$stype} AND vis_ord_num > 0  AND vis_ord_num < 99 ORDER BY vis_ord_num ASC ";
      
      $str = $this->db->query($q)->result_array();
      foreach ($str as $key => $value) {
         if($str[$key]['label'] == 'atom_id'){$str[$key]['value'] = $id;}
         if($str[$key]['label'] == 'monto_cuota_actual'){$str[$key]['value'] = $s->get_pcle('recurrente_monto')->value;}
         
      }
      return $str;
   }
}