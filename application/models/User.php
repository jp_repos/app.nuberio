<?php
Class User extends CI_Model
{
	function login($usuario, $clave)
	{
		$q = "SELECT * FROM usuarios WHERE usr_usuario = '{$usuario}' AND clave_usuario = '{$clave}' LIMIT 1";
		$x = $this -> db -> query($q);
		if(! empty($x))
		{
			return $x->result();
		}
		else
		{
			return false;
		}
	}
	


	function login_cli($user_dni)
	{
		$x = $this -> db -> query(
			"SELECT cap.value as apellido, cnm.value as nombre, c.value as dni,c.atom_id as user_id, e.elements_id,l.name as lote_name FROM atoms_pcles c
			LEFT OUTER join atoms_pcles cap ON cap.label = 'apellido'AND cap.atom_id = c.atom_id
			LEFT OUTER join atoms_pcles cnm ON cnm.label = 'nombre' AND cnm.atom_id = c.atom_id
			LEFT OUTER JOIN elements_pcles e on e.label = 'cli_id' AND e.value = c.atom_id
			LEFT OUTER JOIN elements_pcles et on et.label LIKE '%titular%'AND et.value = c.atom_id
			LEFT OUTER JOIN elements_pcles el on el.label = 'prod_id' AND el.elements_id = e.elements_id
			LEFT OUTER JOIN atoms l on l.id = el.value
			WHERE c.label = 'dni' AND c.value = {$user_dni} AND e.elements_id != 'NULL' LIMIT 1");
		if(! empty($x))
		{
			return $x->result();
		}
		else
		{
			return false;
		}
	}
	
	function u_data($id){
		// $this -> db -> select('nombre, apellido, email, direccion, ciudad, provincia, idpais');
		// $this -> db -> from('usuarios');
		// $this -> db -> where("id_usuario = " . "'" . $id. "'");
		// $this -> db -> limit(1);

		$q = "SELECT * FROM usuarios WHERE id_usuario = {$id} LIMIT 1 "; 
		$x = $this -> db -> query($q);
		if(! empty($x))
		{
			return $x->result();
		}
		else
		{
			return false;
		}


	}
}
?>
