<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Main extends CI_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('Mdl_main');
    // $this->load->helper('array');
    $this->route = "main/";
    //****  CHEKEA EL USUARIO LOGEADO
    $this->user = $this -> session -> userdata('logged_in');
    if (!is_array($this->user)){
      redirect('login', 'refresh');
    }
  }

  function index() {
    $userAvailableActs = $this->Mdl_main->get_activities($this->user['user_id']);
    $acts = explode(',',$userAvailableActs['elements_id']);
    $var=array(
      'route'=>$this->route,
      'user_id'=>$this->user['user_id'],
      'permisos'=>$this->user['user_permisos'],
      'selects'=>[],
      'locked'=>false,
      'screen'=>$this->Mdl_main->get_menu_items($this->user['user_id']),
      'screen_title'=> 'Nuberio App'
    );
    $this->load->view('header-responsive');
    $this->load->view('screen_view',$var);
  }
  
}
