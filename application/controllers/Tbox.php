<?php
if( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tbox extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->Mdb =& get_instance();
        $this->Mdb->load->database();

    
        //****  CHEKEA EL USUARIO LOGEADO
        $this->user = $this -> session -> userdata('logged_in');
        if (!is_array($this->user)){
            redirect('login', 'refresh');
        }
	
    }
	function index() {
    // ****** DATA PARA CUSTOMIZAR LA CLASE
		$cls_name = 'tbox';
      // ****** RUTA DE ACCESO DEL CONTROLLER
		$route = 'tbox/';
      // ****** ******************

    }

    // VERIFICA SI EXISTE EL LABEL:VALUE  ANTES DE CREAR UN ATOM
    function check_duplicate(){
        $p = $this->input->post('data');
        $q = "SELECT label,value FROM atoms_pcles WHERE label = '{$p['label']}' AND value = '{$p['value']}'";
        $r = $this->Mdb->db->query($q)->row();
        $r = [
            'controller'=>$this->input->post('controller'),
            'method'=>$this->input->post('method'),
            'action'=>'response',
            'title'=>' verificacion de email',
            'target'=>$this->input->post('target'),
            'data'=>$r
          ];
          echo json_encode(array(
            'callback'=>'front_call',
            'param'=>$r
          ));
    }


}
