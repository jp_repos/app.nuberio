<body>
	<main class="container-fluid">
		<div id='slider' class="carousel slide" data-interval="false" data-ride="false" data-pause="true">
			<div id='main_container' class="carousel-item active "></div>
			<div class="carousel-item"><div class="d-flex justify-content-between"><div class="btn btn-primary btn-sm mt-1" onclick=bback()><i class="fe-icon-skip-back"></i></div><span id='navbar_msg_1' class='lead pt-2 pr-2 text-right align-bottom' ></span></div><div id="container_1" class="mt-2 p-1" ></div></div>
			<div class="carousel-item"><div class="d-flex justify-content-between"><div class="btn btn-primary btn-sm mt-1" onclick=bback()><i class="fe-icon-skip-back"></i></div></div><h5 id='navbar_msg_2' class='text-right' ></h5></div><div id="container_2" class="mt-2 p-1"></div></div>
			<div class="carousel-item"><div class="d-flex justify-content-between"><div class="btn btn-primary btn-sm mt-1" onclick=bback()><i class="fe-icon-skip-back"></i></div></div><h5 id='navbar_msg_3' class='text-right' ></h5></div><div id="container_3" class="mt-2 p-1"></div></div>
		</div>
	</main>
	<!-- Modal -->
	<div class="modal fade " id="my_modal" tabindex="-1" role="dialog" aria-labelledby="my_modal" aria-hidden="true">
	  <div class="modal-dialog" id='my_modal_container'>
	    <div class="modal-content">
	      <div class="modal-header">
	        <div class="jp-modal-title" id="my_modal_title"></div>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body" id="my_modal_body"></div>
	      <div class="modal-footer" >
	      	<div class="col-1 d-flex justify-content-start flex-wrap">
						<button type="button" class="btn-warning d-none mr-3 ml-3 " id="delete_button" onclick="front_call({method:'kill_modal_content',sending:false})"><i class="material-icons">delete</i></button>
						<button type="button" class="btn-normal d-none mr-3 ml-3 " id="print_button" onclick="$('#printable_content').printThis()"><i class="material-icons">print</i></button>
	      	</div>
	       	<div class="col d-flex justify-content-start flex-wrap" id='modal-footer-msgs'></div>
					<div class="col d-flex justify-content-end flex-wrap" id='modal-footer-butons'>
						<button type="button" class="btn btn-primary " id="close_button" data-dismiss="modal" onClick="front_call(TOP.curr_close_act)" >Volver</button>
		        <button type="button" class="btn btn-primary ml-1" id="ok_button" onClick="front_call(TOP.curr_ok_act)">Aceptar</button>
					</div>
		  </div>
	    </div>
	  </div>
	</div>
<!-- {
		interval:-1,
		keyboard:false,
		pause:true,
		ride:false,
		wrap:false
	} -->
	<noscript>JavaScript esta deshabilitado </noscript>
	<script type="text/javascript">
		$(document).ready(
			function(){
				window.TOP = <?php echo json_encode(array('route'=>$route,'user_id'=>$user_id,'permisos'=>$permisos,'selects'=>$selects,'locked'=>$locked,'screen'=>$screen,'screen_title'=>$screen_title)); ?>;
				$('#slider').carousel();
				$('#slider').carousel('pause');
				$('#slider').on('slid.bs.carousel', function () {
					$('#slider').carousel('pause');
				});

				$("#main_container").html(menu.create(TOP.screen));
	  			// TOP.history = [];
				TOP.current_page = 0;
				TOP.curr_ok_act = {};
				TOP.curr_close_act = {};
				set_dataTable_lang();
				set_autonumeric_def();
			}
		);
	</script>
</body>
</html>
