<!DOCTYPE html>
<html lang="es">
  <head>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale = 1">
     <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <!--   <meta name="description" content="Lotes Para Todos - App V 1.1">
    <meta name="author" content="BigBot.io">
    <meta property="og:title" content="Lotes Para Todos - App V 1.1" />
  <meta property="og:type" content="web application" />
  <meta property="og:url" content="https://www.bigbot.io/lpt/" />
   --><!-- <meta property="og:image" content="https://www.bigbot.io/lpt/logo_lpt.png" /> -->

  <meta http-equiv="Last-Modified" content="0">
  <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
  <meta http-equiv="Pragma" content="no-cache">

  <!-- <link rel="manifest" href="manifest.json" /> -->
  <!-- <script async  src="https://cdn.rawgit.com/GoogleChrome/pwacompat/v2.0.1/pwacompat.min.js"></script> -->
    <!-- <script async  src="<?php  //base_url() ?>dependencies/local_cdn/pwacompat/v2.0.1/pwacompat.min.js"></script> -->
    <!-- <link rel="icon" href="favicon.png"> -->


    <title>Nuberio App - </title>
  <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="<?php echo base_url() ?>dependencies/local_cdn/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet"> -->

        <!-- STYLE CREATEX  -->
        <!-- Vendor Styles including: Font Icons, Plugins, etc.-->
        <link rel="stylesheet" media="screen" href="<?php  echo base_url()?>static/css/vendor.min.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="<?php  echo base_url()?>static/css/theme.css" id="theme-stylesheet">
        <!-- custom stylesheet-->
        <link rel="stylesheet" href="<?php  echo base_url()?>static/css/custom.css" id="custom-stylesheet">
        <!-- Modernizr-->
        <script src="<?php  echo base_url()?>static/js/modernizr.min.js"></script>


<!-- JAVASCRIPTS -->
   <!--Load JQUERY from Google's network -->
  <script src="https://code.jquery.com/jquery-latest.min.js"></script>

  <!--Load Bootstrap JS -->
  <script  src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <!-- <script  src="<?php echo base_url() ?>dependencies/local_cdn/bootstrap/4.0.0/js/bootstrap.min.js"></script> -->

<!--Load JQUERY Autocomplete  -->
  <script type="text/javascript" src="<?php echo base_url() ?>dependencies/jquery-ui.min.js"></script>
  <link href="<?php echo base_url() ?>dependencies/jquery-ui.min.css" rel="stylesheet">
  </head>
  <body>
    <div class="bs-component">
      <div class="container">
        <?php $t = validation_errors();
        if ($t != ''){
          echo"<div class='alert alert-danger' role='alert'><strong>Warning!</strong>$t</div>";
        }
        ?>
        <div class="row d-flex justify-content-center mt-5">
          <div class="col-md-6 pb-5">
            <img class=" mx-auto d-block" src="images/nuberio-iso.png">
            <form class="jp-form-signin" method="post" accept-charset="utf-8" action="verifylogin">
              <div class="wizard-body pt-2">
                <hr>
                <h3 class="h5 pt-4 pb-2">Acceso Administradores</h3>
                <div class="input-group form-group">
                  <div class="input-group-prepend"><span class="input-group-text"><i class="fe-icon-user"></i></span></div>
                  <input id="usr_usuario" name="usr_usuario"  class="form-control" value="usuario_demo" type="text" placeholder="Nombre de Usuario" required="">
                  <div class="invalid-feedback">Nombre de Usuario no Valido!</div>
                </div>
                <div class="input-group form-group">
                  <div class="input-group-prepend"><span class="input-group-text"><i class="fe-icon-lock"></i></span></div>
                  <input id="clave_usuario" name="clave_usuario" class="form-control" value=9999 type="password" placeholder="Clave" required="">
                  <div class="invalid-feedback">Clave No Valida!</div>
                </div>
                <!-- <div class="d-flex flex-wrap justify-content-between">
                      <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" checked="" id="remember-me">
                        <label class="custom-control-label" for="remember-me">Remember me</label>
                      </div>
                      <a class="navi-link" href="account-password-recovery.html">Forgot password?</a>
                    </div> -->
              </div>
              <div class="wizard-footer text-right">
                <button class="btn btn-primary" type="submit">Ingresar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</body></html>
