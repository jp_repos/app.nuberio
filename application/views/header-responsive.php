<!DOCTYPE html>
<html lang="es" class=" svg supports svgfilters csscalc cssgradients preserve3d svgclippaths svgasimg no-touchevents cssanimations boxsizing csstransforms3d no-csstransformslevel2 csstransitions" >
  <head>
    <meta name="viewport" content="width=device-width, initial-scale = 1">
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <meta name="description" content="Aplicacion Web Multiproposito ">
    <meta name="author" content="Nuberio.com">
    <meta property="og:title" content="Nuberio.com - Aplicacion Web Mutiproposito " />
    <meta property="og:type" content="web application" />
    <meta property="og:url" content="https://app.nuberio.com/" />
    <meta name="apple-mobile-web-app-capable" content="yes" />


    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

  <!-- <link rel="manifest" href="manifest.json" /> -->
  <!-- <script async  src="https://cdn.rawgit.com/GoogleChrome/pwacompat/v2.0.1/pwacompat.min.js"></script> -->
    <!-- <script async  src="<?php  //base_url() ?>dependencies/local_cdn/pwacompat/v2.0.1/pwacompat.min.js"></script> -->
    <!-- <link rel="icon" href="favicon.png"> -->
    <title>Nuberio App</title>



    <!-- icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons&vdt=<?php echo date('Y') ?>"
      rel="stylesheet">


    <!-- bootstrap CSS 4.4.1-->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- ****  Data Tables CSS   ****   -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.20/af-2.3.4/b-1.6.1/b-colvis-1.6.1/b-flash-1.6.1/b-html5-1.6.1/b-print-1.6.1/cr-1.5.2/fc-3.3.0/fh-3.1.6/kt-2.5.1/r-2.2.3/rg-1.1.1/rr-1.2.6/sc-2.0.1/sl-1.3.1/datatables.min.css"/>



    <!-- Bootstrap datepicker -->
    <link href="<?php echo base_url() ?>dependencies/bootstrap-datepicker/datepicker.css?vdt=<?php echo date('Y') ?>" rel="stylesheet" media="all">



    <!-- Custom core CSS -->
    <link rel="stylesheet" type="text/css" href="<?php  echo base_url()?>css/JP2.css" />


    <!-- STYLE CREATEX  -->
    <!-- Vendor Styles including: Font Icons, Plugins, etc.-->
    <link rel="stylesheet" media="screen" href="<?php  echo base_url()?>static/css/vendor.min.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?php  echo base_url()?>static/css/theme.css" id="theme-stylesheet">
    <!-- custom stylesheet-->
    <link rel="stylesheet" href="<?php  echo base_url()?>static/css/custom.css" id="custom-stylesheet">
    <!-- Modernizr-->
    <script src="<?php  echo base_url()?>static/js/modernizr.min.js"></script>



    <!-- DATE PICKER STANDALONE CSS -->
    <link href="<?php echo base_url() ?>dependencies/datepicker-1.5.1-dist/css/bootstrap-datepicker.standalone.css?vdt=<?php echo date('Y') ?>" rel="stylesheet">

     <!-- Bootstrap slider css  -->
    <link href="<?php echo base_url() ?>dependencies/slider/css/slider.css?vdt=<?php echo date('Y') ?>" rel="stylesheet">


    <!-- donut chart style -->
    <!-- <link href="<?php // base_url() ?>css/donut3d.css" rel="stylesheet"> -->



  <!-- JAVASCRIPTS -->
  <!--Load JQUERY from Google's network -->
  <script src="https://code.jquery.com/jquery-latest.min.js?vdt=<?php echo date('Y') ?>"></script>
  <!-- Load JQuery from local cdn -->
  <!-- <script src="<?php echo base_url() ?>dependencies/local_cdn/jquery/jquery-latest.min.js"></script> -->

  <!-- JavaScript (jQuery) libraries,CREATEX plugins and custom scripts-->
  <script src="<?php echo base_url() ?>static/js/moment.js"></script>
  <script src="<?php echo base_url() ?>static/js/vendor.min.js"></script>
  <script src="<?php echo base_url() ?>static/js/theme.min.js"></script>
  <script src="<?php echo base_url() ?>static/js/custom.js"></script>



  <!-- load Popper JS  -->
  <!-- <script src="https://unpkg.com/popper.js/dist/umd/popper.min.js?vdt=<?php echo date('Y') ?>"></script> -->
  <!-- <script src="<?php echo base_url() ?>dependencies/local_cdn/popper_js/popper.min.js"></script> -->

  <!-- DATATABLES / BOOTSTRAP -->
  <!--Load Bootstrap JS 4.4.1 -->
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

  <!-- <script  src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js?vdt=<?php echo date('m/Y') ?>"></script> -->
  <!-- <script  src="<?php echo base_url() ?>dependencies/local_cdn/bootstrap/4.0.0/js/bootstrap.min.js"></script> -->


  <!-- ****  Data Tables JS   ****   -->
  <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.20/af-2.3.4/b-1.6.1/b-colvis-1.6.1/b-flash-1.6.1/b-html5-1.6.1/b-print-1.6.1/cr-1.5.2/fc-3.3.0/fh-3.1.6/kt-2.5.1/r-2.2.3/rg-1.1.1/rr-1.2.6/sc-2.0.1/sl-1.3.1/datatables.min.js"></script>





  <!-- ****  DATA TABLES FILTER YADCF -->
  <!-- <script  src="<?php echo base_url() ?>dependencies/local_cdn/DataTables/Filter_yadcf/jquery.dataTables.yadcf.js?vdt=<?php echo date('Y') ?>"></script> -->

  <!-- Select 2 plugin usado por YADCF (yet another datatables custom filter) -->
  <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css?vdt=<?php echo date('Y') ?>" rel="stylesheet" /> -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js?vdt=<?php echo date('Y') ?>"></script> -->


  <!-- Data Tables date time sorting      -->
  <!-- <script  src="<?php echo base_url() ?>dependencies/local_cdn/moment.js-2.8.4/moment.js?vdt=<?php echo date('Y') ?>"></script> -->

  <script  src="<?php echo base_url() ?>dependencies/local_cdn/DataTables/Ultimate_DateTime_sorting-1.10.19/Ultimate_DateTime_sorting.js?vdt=<?php echo date('Y') ?>"></script>

  <script  src="<?php echo base_url() ?>dependencies/local_cdn/DataTables/Responsive-2.2.3/js/dataTables.responsive.min.js?vdt=<?php echo date('Y') ?>"></script>
    <!-- Data Tables suma columnas   -->
  <script  src="<?php echo base_url() ?>dependencies/local_cdn/DataTables/sum.js?vdt=<?php echo date('Y') ?>"></script>

  <!-- format de numeros  -->
  <script  src="<?php echo base_url() ?>dependencies/local_cdn/accounting/accounting.js?vdt=<?php echo date('Y') ?>"></script>


   <!-- my functions -->
  <script src="<?php echo base_url() ?>js/fnks.js?vdt=<?php echo date('d/m/Y H:m') ?>"></script>
  <script src="<?php echo base_url() ?>js/ob.js?vdt=<?php echo date('d/m/Y H:m') ?>"></script>
  <script src="<?php echo base_url() ?>js/router.js?vdt=<?php echo date('d/m/Y H:m') ?>"></script>
  <script src="<?php echo base_url() ?>js/validator.js?vdt=<?php echo date('d/m/Y H:m') ?>"></script>
  <script src="<?php echo base_url() ?>js/get_resumen_screen_obj.js?vdt=<?php echo date('d/m/Y H:m') ?>"></script>
  <script src="<?php echo base_url() ?>js/main_menu.js?vdt=<?php echo date('d/m/Y H:m') ?>"></script>

   <!-- Bootstrap datepicker js -->
   <script type="text/javascript"  src="<?php echo base_url() ?>dependencies/datepicker-1.5.1-dist/js/bootstrap-datepicker.min.js"></script>


  <!-- <script src="<?php echo base_url() ?>dependencies/bootstrap-datepicker/moment.js?vdt=<?php echo date('Y') ?>"></script>
  <script src="<?php echo base_url() ?>dependencies/bootstrap-datepicker/locale-es.js?vdt=<?php echo date('Y') ?>"></script>
  <script src="<?php echo base_url() ?>dependencies/bootstrap-datepicker/datepicker.js?vdt=<?php echo date('Y') ?>"></script>
  <script src="<?php echo base_url() ?>dependencies/bootstrap-confirmation.js?vdt=<?php echo date('Y') ?>"></script> -->

<!-- PrintThis js -->
  <script type="text/javascript"  src="<?php echo base_url() ?>dependencies/local_cdn/printThis/printThis.js?vdt=<?php echo date('Y') ?>"></script>


  <!-- Bootstrap slider js -->
  <script type="text/javascript"  src="<?php echo base_url() ?>dependencies/slider/js/bootstrap-slider.js?vdt=<?php echo date('Y') ?>"></script>

 <!-- <script src="https://d3js.org/d3.v4.min.js"></script> -->
<!-- <script type="text/javascript"  src="<?php echo base_url() ?>dependencies/local_cdn/d3/d3.v4.min.js?vdt=<?php echo date('Y') ?>"></script> -->

  <!-- Bootstrap d3 min -->
  <!-- <script type="text/javascript"  src="<?php  //base_url() ?>dependencies/d3/d3.min.js"></script> -->
  <!-- d3 charts  -->
  <!-- <script type="text/javascript"  src="<?php  //base_url() ?>dependencies/d3/charts/donut3D.js"></script> -->

  <!-- Add block ui loader and block window plugin -->
  <script type="text/javascript" src="<?php echo base_url() ?>dependencies/blockUI_plugin.js?vdt=<?php echo date('Y') ?>"></script>


<!--Load JQUERY Autocomplete  -->
  <script type="text/javascript" src="<?php echo base_url() ?>dependencies/jquery-ui.min.js?vdt=<?php echo date('Y') ?>"></script>
  <link href="<?php echo base_url() ?>dependencies/jquery-ui.min.css?vdt=<?php echo date('Y') ?>" rel="stylesheet">

<!-- load AutoNumeric -->
<!-- <script src="<?php //echo base_url() ?>dependencies/local_cdn/AutoNumeric/AutoNumeric.js" type="text/javascript"></script>
<script src="<?php //echo base_url() ?>dependencies/local_cdn/AutoNumeric/AutoNumericHelper.js" type="module"></script>
<script src="<?php //echo base_url() ?>dependencies/local_cdn/AutoNumeric/AutoNumericEnum.js" type="module"></script>
<script src="<?php //echo base_url() ?>dependencies/local_cdn/AutoNumeric/maths/Evaluator.js" type="module"></script>
<script src="<?php //echo base_url() ?>dependencies/local_cdn/AutoNumeric/maths/Parser.js" type="module"></script>
 -->
<script src="https://cdn.jsdelivr.net/npm/autonumeric@4.1.0?vdt=<?php echo date('Y') ?>"></script>


</head>
