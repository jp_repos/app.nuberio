// PANTALLA REUMEN DE CUENTA
var rdc = {
	_data:{},
	_screen:'',
	create:function(val){
		var obj = Object.create(this);
		obj.set(val);
		return obj;
	},
	set: function(val){
		TOP.data = val;
		TOP.detalle_ctas_arr = new Array();
		TOP.titulo_det_ctas_arr = new Array();
		TOP.detalle_ctas_a_pagar_arr = new Array();
		TOP.titulo_det_ctas_a_pagar_arr = new Array();
		// // console.log('TOP data',TOP.data)
		this._data = val.data;

		//****** PANTALLA
		this._screen = this.get_card1(); // + this.get_card2(); //+ (this.get_card_canclds()?this.get_card_canclds():'');


	},
	get_fec_init : function(){
		let r = "<div id=\'btns_pago_srv\' class=\'d-flex flex-wrap p-2 \'>";
		// r += "<div class=\"btn-group p-1\" role=\"group\" aria-label=\"Button group with nested dropdown\">\
		r +="<button type=\"button\" id=\"btn_curr_state\" class=\"btn btn-success\">"+this._data.lote['fec_init']+"</button>";
		// </div>";
		// return "<span class=\"badge badge-"+type+" badge-pill \">"+x+"</span></li></a>";
		r += "</div>";
		return r;
	},
	get_curr_state : function(){
		var type = '';
		var x = '';
		switch(this._data.lote.curr_state){
			case 'NORMAL':
			type = 'success';
			x = 'Normal';
			break;
			case 'EN_REVISION':
			type = 'danger';
			x = 'A Revisar';
			break;
			case 'REVISADO':
			type = 'success';
			x = 'Revisado';
			break;

			case 'EN_RESCISION':
			type = 'warning';
			x = 'En Rescision';
			break;
			case 'RESCINDIDO':
			type = 'danger';
			x = 'Rescindido';
			break;
			case 'EN_LEGALES':
			type = 'danger';
			x = 'Legales';
			break;
			case 'EN_ACTUALIZACION':
			type = 'warning';
			x = 'EN Actualización';
			break;
			case 'ACTUALIZADO':
			type = 'success';
			x = 'Actualizado';
			break;
		}
		// *** SELECTOR DE ESTADO
		let r = "<div id=\'btns_sect_estado\' class=\'d-flex flex-inline flex-wrap p-2 \'>";
		r += "<div class=\"btn-group p-1\" role=\"group\" aria-label=\"Button group with nested dropdown\">";
		r += "<button type=\"button\" id=\"btn_curr_state\" class=\"btn btn-"+type+"\">"+x+"</button>";
		r += "<div class=\"btn-group show\" role=\"group\">";
		r += "<button id=\"btnGroupDrop1\" type=\"button\" class=\"btn btn-"+type+" dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\"></button>";
		r += "<div class=\"dropdown-menu \" aria-labelledby=\"btnGroupDrop1\" x-placement=\"bottom-start\" style=\"position: absolute; transform: translate3d(0px, 36px, 0px); top: 0px; left: 0px; will-change: transform; z-index:10000;\">";
		// r += "<a class=\"dropdown-item\" id=\'state_item_1\' href=\"#\" onClick=front_call({'method':'set_curr_state',sending:true,'state':'EN_REVISION'})>A Revisar</a>";
		r += "<a class=\"dropdown-item\" id=\'state_item_2\' href=\"#\" onClick=front_call({'method':'set_curr_state',sending:false,'state':'EN_RESCISION'})>En Rescisión</a>";
		r += "<a class=\"dropdown-item\" id=\'state_item_3\' href=\"#\" onClick=front_call({'method':'set_curr_state',sending:true,'state':'EN_LEGALES'})>En Legales</a>";
		r += "<a class=\"dropdown-item\" id=\'state_item_2\' href=\"#\" onClick=front_call({'method':'actualizar_contrato',sending:true,action:'call','state':'EN_ACTUALIZACION'})>Actualizar</a>";
		r += "</div></div></div></div>";
		return r;
	},


	get_header : function(){
		let r = "";
		console.log('heder',this._data);
		r += "<div class='col d-flex flex-wrap card-title justify-content-center'><i class=\"material-icons \">perm_identity</i>" + this._data.cli_nombre	+"</div>";
		r += "<div class='col d-flex flex-wrap  card-title justify-content-center'><i class=\"material-icons \">phone</i>"+this._data.cli_tel+"</div>";
		// r += "<div class='col d-flex flex-wrap card-title justify-content-center'><i class=\"material-icons \">home</i>"+this.get_domic()+"</div>";
		return r;
	},
	get:function(card,prop){
		if(this._data.hasOwnProperty(card) && this._data[card].hasOwnProperty(prop)){
			return this._data[card][prop];
		}
	},
	get_screen:function(){
		return this._screen
	},
	get_plan : function (){
		return this._data.lote.financ;
	},
	get_pcle : function (card,p,lbl){
		if(this._data.hasOwnProperty(card) && this._data[card].hasOwnProperty(p) && this._data[card][p].length > 0){
			var r = this._data[card][p].filter(function(i){return i.label === lbl});
			if(r.length >0){
				return r[0]['value'];
			}
		}
	},
	get_gpcle : function (arr,p,lbl){

		if(arr && arr.hasOwnProperty(p) && arr[p].length > 0){
			var r = arr[p].filter(function(i){return i.label === lbl});
			if(r.length >0){
				return r[0]['value'];
			}
		}
	},
	get_title : function (c,p,t){
		if(this._data[c].hasOwnProperty(p) && this._data[c][p].length > 0){
			var r = this._data[c][p].filter(function(i){return i.label === t});
			if(r.length >0){
				return r[0]['title'];
			}

		}
	},

	get_nro_cta : function(){
		if(this._data.lote['cta_upc'].length > 0 && this._data.lote['cta_upc'][0].hasOwnProperty('cuota') && this._data.lote['cta_upc'][0]['cuota'].hasOwnProperty('id')){
			var x = this._data.lote['cta_upc'][0]['pcles'].filter(function(i){return i.label == 'nro_cta'});
			return x[0].value;
		}else {
			return ' ';
		}
	},
	get_cta_upc : function(){
		return (this._data.lote.hasOwnProperty('cta_upc') && this._data.lote.cta_upc !== 0 ? parseFloat(this._data.lote.cta_upc.pcles.monto.value):0);
	},
	get_cant_ctas_restantes : function(){
		return this._data.lote.ctas_restantes.events.length;
	},
	get_cant_ctas_pagas : function(){
		return parseInt(this._data.lote.ctas_pagas.events.length) ;
	},
	get_cant_ctas_pftrm :  function(){
		return this._data.lote.ctas_pft.events.length;
	},
	get_cant_ctas_adelant:function(){
		return this._data.lote.ctas_adelantadas.events.length;
	},
	get_cant_ctas_mora : function(){
		return this._data.lote.ctas_mora.events.length;
	},
	get_tot_en_mora : function(){
		return parseInt(this._data.lote.ctas_mora.total);
	},
	get_tot_restantes :function(){
		return parseInt(this._data.lote.ctas_restantes.total);
	},
	get_cant_pagas : function(e,caller){
		let r = 0;r1='',r2='';r3='';
		d = new Array();
		ptrm = e.ctas_pagas.events.length + e.ctas_adelantadas.events.length;
		ftrm = e.ctas_pft.events.length;
		r1 = ptrm +  ftrm + " Ctas. Pagadas";
		if(ptrm > 0){
			r2 = "<br/><span class=\'small text-success\' >"+ ptrm + " en Termino</span>";
		}
		if(ftrm > 0){
			r3 = "<br/><span class=\'small text-danger\'> "+ftrm+" fuera de termino </span>";
		}

		d = e.ctas_pagas.events.concat(e.ctas_adelantadas.events,e.ctas_pft.events);

		ax = TOP.detalle_ctas_arr.push(ordenar_por_fecha(d));

		TOP.titulo_det_ctas_arr[ax-1] = caller;
		ro = {method:'detalle_ctas',det_arr_index:ax-1};
		let res = "<div class=\'text-left\'><a href=\"#\" onClick=front_call("+JSON.stringify(ro)+")>"+r1+"</a>"+' '+r2+' '+r3+"</div>";
		return res;
	},
	get_tot_pagado : function(e){
		let t = e.ctas_pagas.total + e.ctas_adelantadas.total + e.ctas_pft.total
		return (isNaN(t)?'err':t);
	},
	get_tot_a_pagar : function(e){
		let x = 0;c = 0;a=0;
		if(e.cta_upc.total > 0){
			console.log('xx',e.cta_upc.events[e.cta_upc.events.length-1].ord_num)
			// MONTO DE CTA ES ULTIMO ELEMENTO DE UPCOMING
			c += parseInt(get_pcle(e.cta_upc.events[(e.cta_upc.events.length-1)],'monto_cta'));
			x = e.ctas_mora.total + (c * e.ctas_restantes.events.length) ;
			// SI ES CTA ANTICIPO O CERO, MONTO CTA ES DEL PRIMER ELEMENTO DE RESTANTES
			if(e.cta_upc.events[e.cta_upc.events.length-1].ord_num === '0.0' || c == 0){
					a = parseInt(get_pcle(e.ctas_restantes.events[0],'monto_cta'))
					c = parseInt(get_pcle(e.ctas_restantes.events[1],'monto_cta'));
					x = a + (c * e.ctas_restantes.events.length - 1) ;
			}
		}else{
			if(e.ctas_restantes.total > 0){
				c = parseInt(get_pcle(e.ctas_restantes.events[0],'monto_cta'));
				x = e.ctas_mora.total + (c * e.ctas_restantes.events.length) ;
			}
		}

		return (isNaN(x)?'error - 2345 get_tot_a_pagar':x);
	},

	get_cant_a_pagar : function(e,caller){
		let dta = new Array(); x1='';x2='';x3='';

		x1 = e.ctas_mora.events.length + e.ctas_restantes.events.length
		if(e.ctas_restantes.events.length > 0){
			x2 = "<br/><span class=\'small text-success\'> "+e.ctas_restantes.events.length+" en fecha </span>";
		}
		if(e.ctas_mora.events.length > 0){
			x3 = "<br/><span class=\'small text-danger\'> "+e.ctas_mora.events.length+" en mora </span>";
		}

		dta = e.ctas_mora.events.concat(e.ctas_restantes.events);
		ax = TOP.detalle_ctas_arr.push(dta);
		TOP.titulo_det_ctas_arr[ax-1] = caller;
		ret_obj = {method:'detalle_ctas',det_arr_index:ax-1};
		return "<a href=\"#\" onClick=front_call("+JSON.stringify(ret_obj)+")>"+x1+" Ctas. A Pagar</a>"+x2+x3;
	},
	get_monto_cta_actual(c){
		if(c.ctas_restantes.events.length > 0){
			let r = c.ctas_restantes.events[0]['pcles'].filter(function(i){return i.label === 'monto_cta'});
			return parseFloat(r[0]['value']);
		}else{
			if(c.cta_upc.total > 0){
				return c.cta_upc.total;
			}
			return 0;
		}
	},
	ctas_lote: function(){
		TOP.ahorrado_tot = this._data.lote.ctas_ahorro;
		let tbl_data_lote = [{
			'PLAN CONTRATADO' : this.get_plan(),
			'AHORRO':this._data.lote.ctas_ahorro,
			'VALOR DE LA CUOTA' : this.get_monto_cta_actual(this._data.lote),
			'CUOTAS PAGADAS' : this.get_cant_pagas(this._data.lote,this.get_plan()),
			'MONTO PAGADO' : this.get_tot_pagado(this._data.lote),
			'CUOTAS A PAGAR' : this.get_cant_a_pagar(this._data.lote,this.get_plan()),
			'MONTO A PAGAR': this.get_tot_a_pagar(this._data.lote),

			// 'Limite Cred.' : this._data.lote.mto_reintegro,
			// 'ACCIONES': this._data.lote.ctas_acciones
			//'En Mora':this.get_tot_en_mora(),
			//'Fuera Term.':this.get_cant_ctas_pftrm()
			}];
		if(TOP.permisos < 10){
			tbl_data_lote[0]['ACCIONES'] = this._data.lote.ctas_acciones;
		}
		return otbl.create(tbl_data_lote,'tbl_ctas_lote');

	},
	ctas_srv: function(){
		let tbl_data_srv = [];
		for (let i = 0 ; i < this._data.srv.length ; i ++){
			if(this._data.srv[i].srvc_name.indexOf('Prestamo') == -1 ){
				if(this._data.srv[i].ctas_restantes.events.length > 0 || this._data.srv[i].ctas_mora.events.length > 0 ){
					TOP.ahorrado_tot += this._data.srv[i].ctas_ahorro;
					var ds = tbl_data_srv.push({
						'SERVICIO CONTRATADO' : this._data.srv[i].srvc_name,
						'AHORRO':this._data.srv[i].ctas_ahorro,
						'VALOR DE LA CUOTA' : this.get_monto_cta_actual(this._data.srv[i]),
						'CUOTAS PAGADAS' : this.get_cant_pagas(this._data.srv[i],(this._data.srv[i].srvc_name?(this._data.srv[i].srvc_name).substring(0,25):'')),
						'MONTO PAGADO' : this.get_tot_pagado(this._data.srv[i]),
						'CUOTAS A PAGAR' : this.get_cant_a_pagar(this._data.srv[i],(this._data.srv[i].srvc_name?(this._data.srv[i].srvc_name).substring(0,25):'')),
						'MONTO A PAGAR' : this.get_tot_a_pagar(this._data.srv[i]),
					});
					if(TOP.permisos < 10){
						tbl_data_srv[ds-1]['ACCIONES'] = this._data.srv[i].ctas_acciones;
					}

				}
			}

		}
		// if(TOP.permisos < 10){
		// 	tbl_data_lote[0]['ACCIONES'] = this._data.lote.ctas_acciones;
		// }

		return otbl.create(tbl_data_srv,'tbl_ctas_srv');

	},
	ventas_list: function(){
		let v = []; let gtot = 0;
		for(let i = 0 ; i < this._data.ventas.length ; i ++){
			let cpv = this._data.prods_vendidos.map(function(p){return parseInt(p.Cantidad)}).reduce(function(a, b){ return a + b; },0);
			v.push({
				'Fecha':this._data.ventas[i].fecha,
				'Vendedor':this._data.ventas[i].vendedor,
				"Cant. Prods. Vendidos":this._data.ventas[i].cant_productos,
				'Total Venta': this._data.ventas[i].total,
				'Acción':{method:'delete',id:this._data.ventas[i].id,origin:'resumen_de_cta'}
			});
		}
		return otbl.create(v,'tbl_ventas');
	},

	ctas_prest: function(){
		let tbl_data_prest = [];
		for (let i = 0 ; i < this._data.srv.length ; i ++){
			if(this._data.srv[i].srvc_name.indexOf('Prestamo') == 0 ){
				if(this._data.srv[i].ctas_restantes.events.length > 0 || this._data.srv[i].ctas_mora.events.length > 0 ){

					TOP.ahorrado_tot += this._data.srv[i].ctas_ahorro;
					var dp = tbl_data_prest.push({
						'SERVICIO CONTRATADO' : this._data.srv[i].srvc_name,
						'AHORRO':this._data.srv[i].ctas_ahorro,
						'VALOR DE LA CUOTA' : this.get_monto_cta_actual(this._data.srv[i]),
						'CUOTAS PAGADAS' : this.get_cant_pagas(this._data.srv[i],(this._data.srv[i].srvc_name?(this._data.srv[i].srvc_name).substring(0,25):'')),
						'MONTO PAGADO' : this.get_tot_pagado(this._data.srv[i]),
						'CUOTAS A PAGAR' : this.get_cant_a_pagar(this._data.srv[i],(this._data.srv[i].srvc_name?(this._data.srv[i].srvc_name).substring(0,25):'')),
						'MONTO A PAGAR' : this.get_tot_a_pagar(this._data.srv[i]),
					});
					if(TOP.permisos < 10){
						tbl_data_prest[dp-1]['ACCIONES'] = this._data.srv[i].ctas_acciones;
					}
				}
			}
		}
		if(tbl_data_prest.length > 0){
			let tit_prest = "<div class='title-prestamos'><img class=\'jp-icon\' src=\'images/icons/prestamos.png\'></img> ESTADO DE CUENTA DE"+(TOP.permisos >= 10 ?" TUS ":" ")+"PRESTAMOS</div>";

			return tit_prest + otbl.create(tbl_data_prest,'tbl_ctas_prest');
		}else{
			return "";
		}
	},

	// ** DEPRECATED CTAS TBL
	ctas_tbl : function(){
		let tbl_data = [{
			'Plan / Servicio' : this.get_plan(),
			'Cant. Pagas' : this.get_cant_pagas(this._data.lote,this.get_plan()),
			'Monto Pagado' : this.get_tot_pagado(this._data.lote),
			'Cant. a Pagar' : this.get_cant_a_pagar(this._data.lote,this.get_plan()),
			'Monto a Pagar': this.get_tot_a_pagar(this._data.lote),
			'Monto Cta. Actual' : this.get_monto_cta_actual(this._data.lote),
			// 'Limite Cred.' : this._data.lote.mto_reintegro,
			'Acciones': this._data.lote.ctas_acciones
			//'En Mora':this.get_tot_en_mora(),
			//'Fuera Term.':this.get_cant_ctas_pftrm()
			}];
		let gtot_pagado = this.get_tot_pagado(this._data.lote);
		gtot_cant_pagas = this._data.lote.ctas_pagas.events.length + this._data.lote.ctas_adelantadas.events.length + this._data.lote.ctas_pft.events.length;
		gtot_a_pagar = this.get_tot_a_pagar(this._data.lote);
		gtot_cant_a_pagar = this._data.lote.ctas_mora.events.length + this._data.lote.ctas_restantes.events.length;
		gtot_cta_actual = this.get_monto_cta_actual(this._data.lote);
		for (let i = 0 ; i < this._data.srv.length ; i ++){
			if(this._data.srv[i].ctas_restantes.events.length > 0 || this._data.srv[i].ctas_mora.events.length > 0 ){
				gtot_pagado += this.get_tot_pagado(this._data.srv[i])
				gtot_cant_pagas += this._data.srv[i].ctas_pagas.events.length + this._data.srv[i].ctas_adelantadas.events.length + this._data.srv[i].ctas_pft.events.length;
				gtot_a_pagar += this.get_tot_a_pagar(this._data.srv[i]),
				gtot_cant_a_pagar += this._data.srv[i].ctas_mora.events.length + this._data.srv[i].ctas_restantes.events.length,
				gtot_cta_actual += this.get_monto_cta_actual(this._data.srv[i]),
				tbl_data.push({
					'Plan / Servicio' : this._data.srv[i].srvc_name,
					'Cant. Pagas' : this.get_cant_pagas(this._data.srv[i],(this._data.srv[i].srvc_name?(this._data.srv[i].srvc_name).substring(0,25):'')),
					'Monto Pagado' : this.get_tot_pagado(this._data.srv[i]),
					'Cant. a Pagar' : this.get_cant_a_pagar(this._data.srv[i],(this._data.srv[i].srvc_name?(this._data.srv[i].srvc_name).substring(0,25):'')),
					'Monto a Pagar' : this.get_tot_a_pagar(this._data.srv[i]),
					'Monto Cta. Actual' : this.get_monto_cta_actual(this._data.srv[i]),
					// 'Limite Cred.' : 0,
					'Acciones': this._data.srv[i].ctas_acciones
					// 'En Mora':this._data.srv[i].ctas_mora.events.length,
					// 'Fuera Term.':this._data.srv[i].ctas_pft.events.length,
				});
			}
		}
		if(tbl_data.length > 1){
			tbl_data.push({
				'Plan / Servicio' : 'TOTALES',
				'Cant. Pagas' : gtot_cant_pagas,
				'Monto Pagado' : gtot_pagado,
				'Cant. a Pagar' : gtot_cant_a_pagar,
				'Monto a Pagar' : gtot_a_pagar,
				'Monto Cta. Actual' : gtot_cta_actual,
				// 'Limite Cred.' : 0,
				'Acciones' : ' '
					// 'En Mora':this._data.srv[i].ctas_mora.events.length,
					// 'Fuera Term.':this._data.srv[i].ctas_pft.events.length,
				});

		}
		// console.log('actions',tbl_data);
		return otbl.create(tbl_data,'tbl_ctas');
	},

	//  ******************* ctas tble no actions ************
	ctas_tbl_noacc : function(){
		let tbl_data = [{
			'Plan / Servicio' : (this.get_plan()?(this.get_plan()).substring(0,25):''),
			'Cant. Pagas' : this.get_cant_pagas(this._data.lote,(this.get_plan()).substring(0,25)),
			'Monto Pagado' : this.get_tot_pagado(this._data.lote),
			'Cant. a Pagar' : this.get_cant_a_pagar(this._data.lote,(this.get_plan()).substring(0,25)),
			'Monto a Pagar': this.get_tot_a_pagar(this._data.lote),
			'Monto Cta. Actual' : this.get_monto_cta_actual(this._data.lote)
			}];
		let gtot_pagado = this.get_tot_pagado(this._data.lote);
		gtot_cant_pagas = this._data.lote.ctas_pagas.events.length + this._data.lote.ctas_adelantadas.events.length + this._data.lote.ctas_pft.events.length;
		gtot_a_pagar = this.get_tot_a_pagar(this._data.lote);
		gtot_cant_a_pagar = this._data.lote.ctas_mora.events.length + this._data.lote.ctas_restantes.events.length;
		gtot_cta_actual = this.get_monto_cta_actual(this._data.lote);
		for (let i = 0 ; i < this._data.srv.length ; i ++){
			if(this._data.srv[i].ctas_restantes.events.length > 0 || this._data.srv[i].ctas_mora.events.length > 0 ){
				gtot_pagado += this.get_tot_pagado(this._data.srv[i])
				gtot_cant_pagas += this._data.srv[i].ctas_pagas.events.length + this._data.srv[i].ctas_adelantadas.events.length + this._data.srv[i].ctas_pft.events.length;
				gtot_a_pagar += this.get_tot_a_pagar(this._data.srv[i]),
				gtot_cant_a_pagar += this._data.srv[i].ctas_mora.events.length + this._data.srv[i].ctas_restantes.events.length,
				gtot_cta_actual += this.get_monto_cta_actual(this._data.srv[i]),
				tbl_data.push({
					'Plan / Servicio' : (this._data.srv[i].srvc_name)?(this._data.srv[i].srvc_name).substring(0,25):'',
					'Cant. Pagas' : this.get_cant_pagas(this._data.srv[i],(this._data.srv[i].srvc_name?(this._data.srv[i].srvc_name).substring(0,25):'')),
					'Monto Pagado' : this.get_tot_pagado(this._data.srv[i]),
					'Cant. a Pagar' : this.get_cant_a_pagar(this._data.srv[i],(this._data.srv[i].srvc_name?(this._data.srv[i].srvc_name).substring(0,25):'')),
					'Monto a Pagar' : this.get_tot_a_pagar(this._data.srv[i]),
					'Monto Cta. Actual' : this.get_monto_cta_actual(this._data.srv[i])
				});
			}
		}
		if(tbl_data.length > 1){
			tbl_data.push({
				'Plan / Servicio' : 'TOTALES',
				'Cant. Pagas' : gtot_cant_pagas,
				'Monto Pagado' : gtot_pagado,
				'Cant. a Pagar' : gtot_cant_a_pagar,
				'Monto a Pagar' : gtot_a_pagar,
				'Monto Cta. Actual' : gtot_cta_actual
				});

		}

		return otbl.create(tbl_data,'tbl_ctas_noacc');
	},
	//  ***************************************************


	//  ******************* botones Ingresar Pago /  New servicio / Print resumende cuenta  ************
	get_buttons_bar : function(){
		let r = "";
		//*** LOTE RESCINDIDO
		// if(this._data.lote.rscn_data && this._data.lote.curr_state == 'RESCINDIDO'){
		// 	r +="<div class='row d-flex justify-content-between mb-2'>" ;
		// 	//  BOT SUBIR ARCHIVO
		// 	r += "<button type='button' class='btn-normal m-1 ' id='button_file_upload' onClick=front_call({method:'lotes_file_upload',sending:false})>SUBIR ARCHIVO</button>";
		// 	// BOT IMPRIMIR
		// 	r +="<button type='button' class='btn-normal' id='print_button_res_cta' onclick=\"print_resumen_de_cta()\">IMPRIMIR EL ESTADO DE CUENTA</button>";
		// 	r += "</div>";
		// 	return r;
		// }
		//**** PERMISOS USUARIOS OFICINA SOLO SUBIR ARCHIVOS O IMPRIMIR
		// if(TOP.permisos == 3){
		// 	r +="<div class='row d-flex justify-content-between mb-2'>";
		// 	//  BOT SUBIR ARCHIVO
		// 	r += "<button type='button' class='btn-normal m-1 ' id='button_file_upload' onClick=front_call({method:'lotes_file_upload',sending:false})>SUBIR ARCHIVO</button>";
		// 	// BOT IMPRIMIR
		// 	r +="<button type='button' class='btn-normal' id='print_button_res_cta' onclick=\"print_resumen_de_cta()\">IMPRIMIR EL ESTADO DE CUENTA</button>";
		// 	r += "</div>";
		// 	return r;
		// }



		//**** PERMISOS ADMINISTRACION SIN CUOTAS EN MORA
		if(TOP.permisos < 5 ){
			r +="<div class='row d-flex justify-content-start mb-2'>";
			// DISPONIBLE DE CREDITO
			// r +="<div class\'col-sm-4 col-md-2 \'><div class=\' p-3 text-center\'>Credito Disponible: "+accounting.formatMoney(parseFloat(this._data.lote.mto_reintegro), "$ ", 0, ".", ",")+"</div></div>";
			// r += "<div class\'col-sm-8 col-md-10 \'>"
			// BOT de PAGOS
			r += "<button type=\"button\" id=\"bot_pago\" class=\"btn-normal m-1\" onClick=front_call({method:'ingresar_pago',sending:false})>Ingresar Pagos del Cliente </button>";
			// BOT SERVICIO
			// r += "<button type=\"button\" id=\"bot_new_service\" class=\"btn-normal m-1\" onClick=front_call({method:'new_service_elem',action:'call',sending:true})>NUEVO SERVICIO</button>";
			// BOT REFINANCIAR
			// r += "<button type=\"button\" id=\"bot_new_service\" class=\"btn-normal m-1\" onClick=front_call({method:'refinanciar',action:'call',sending:false})>REFINANCIAR CUOTA</button>";
			// r += "<div class='row d-flex justify-content-start mb-2'>";
			//  BOT SUBIR ARCHIVO
			// r += "<button type='button' class=\'btn-normal\' id='button_file_upload' onClick=front_call({method:'lotes_file_upload',sending:false})>SUBIR ARCHIVO</button>";
			// BOT COMUNICADOS INTERNOS
			// r += "<button type='button' class=\'btn-normal\' id='send_msg' onClick=front_call({method:'send_msg',sending:false,action:'observacion'})>NUEVO MENSAGE</button>";
			// BOT IMPRIMIR
			// r +="<button type='button' class='btn-normal m-1' id='print_button_res_cta' onclick=\"print_resumen_de_cta()\">IMPRIMIR EL ESTADO DE CUENTA</button>";
			r += "</div></div>";
			return r;

		}

		//**** PERMISOS USUARIOS VENTAS Y WEB CLI
		if(TOP.permisos >= 10){
			// ROW BOTON DE PAGOS
			// r += "<div class=\'row text-center mb-2\'>"
			// r +="<div class=\'col\'>"
			// r += "<a href=\'#\'><button type=\"button\" id=\"bot_pago\" class=\"btn-normal \" onClick=front_call({method:'set_pago_cuotas',sending:'true',action:'call',steps_back:true})>PAGAR CUOTAS ONLINE</button></a><br/>";
			// r +="<div class=\' pl-3 pr-3 text-center small\'>Adelanta cuotas y obtené un descuento sobre tu plan, llevás ahorrado: "+accounting.formatMoney(parseFloat(TOP.ahorrado_tot), "$ ", 0, ".", ",")+"</div>";
			// r +="</div></div>"
			// CLOSE ROW
			// ROW CONSULTAS
			// r += "<hr/>";
			// r +="<div class='row d-flex justify-content-between mb-3'>";
			// r +="<div class=\'col d-flex flex-wrap justify-content-around\'>"
			// r +="<a href=\'http://api.whatsapp.com/send?phone=5491145260488&amp;text=Hola%20quiero%20mas%20info%20sobre%20un%20prestamo%20que%20vi%20en%20la%20web%20de%20LotesParaTodos\' target='_blank'><button type='button' class='btn-prestamo' id='button_whatsapp' onclick=\"\">CONSULTANOS POR WHATSAPP YA!</button></a>"
			// r +="<div class=\' pl-3 pr-3 text-center small\'>Tenés preaprobado un préstamo de hasta: "+accounting.formatMoney(parseFloat(this._data.lote.mto_reintegro), "$ ", 0, ".", ",")+"</div>";
			// r +="</div>";
			// r +="<div class=\'col d-flex flex-wrap justify-content-around\'>"
			// r += "<a href=\'#\'><button type='button' class='btn-normal ' id='button_file_upload' onClick=front_call({method:'cli_file_upload',sending:false})>ENVIAR COMPROBANTE DE PAGO</button></a>";
			// r +="<div class=\' pl-3 pr-3 text-center small\'>Subí tu comprobante de pago para actualizar tu cuenta.</div>";
			// r +="</div>"
			// r += "<button type='button' class='btn-secondary m-2 ' id='button_cli_file_upload' onClick=front_call({method:'send_msg',sending:false,action:'observacion'})>Comunicate con nosotros</button>";
			// r += "</div>";
			// r +="<div class=\'col d-flex flex-wrap justify-content-around\'>"
			// r +="<a href=\'#\'><button type='button' class='btn-normal' id='print_button_res_cta' onclick=\"print_resumen_de_cta()\">IMPRIMÍ EL ESTADO DE CUENTA</button></a>";
			// r +="<div class=\' pl-3 pr-3 text-center small\'>Descargá el estado de cuenta y guardalo o imprimilo.</div>";
			// r +="</div></div><hr/>"
			// return r;

		}


	},


		//  *** SERVICIOS CARD
	service_cards : function(){
		var r = '';
		for (var i = 0 ; i < this._data.srv.length ; i ++){
			r +="<div id=\'srv_card_"+i+"\' class=\'row \'>"
			r +="<div class = \"panel panel-primary\">";
			r += "<div class = \"panel-heading d-flex flex-wrap justify-content-around\">";
			r += "<a data-toggle = \"collapse\" href = \"#srv_row_"+i+"\"><button type=\"button\" class=\"btn-normal \" ><i class=\"material-icons \">more_vert</i></button></a>";
			r += "<span class=\'d-flex flex-wrap p-2\'>Servicio: "+this._data.srv[i].srvc_name+"</span>";
			r += "<span class=\'d-flex flex-wrap p-2\'>Tot. Pagado:"+accounting.formatMoney(parseFloat(this._data.srv[i].tot_pagado), "$", 0, ".", ",")+"</span>";
			r += "<span class=\'d-flex flex-wrap p-2\'>Cuotas a Pagar:"+this._data.srv[i].ctas_restantes.events.length+"</span>";
			r += "<span class=\'d-flex flex-wrap p-2\'>Monto $:"+accounting.formatMoney(parseFloat(this._data.srv[i].ctas_restantes.total), "$", 0, ".", ",")+"</span>";
			r += "<span class=\'d-flex flex-wrap p-2\'>Cuota Actual $:"+this.get_monto_cta_actual(this._data.srv[i].ctas_restantes.events)+"</span>";
			r += "</div>";
			r +="<div id = \"srv_row_"+i+"\" class=\"panel-collapse collapse\"><ul class = \"list-group\">";
			r += "<a href=\'#\' onClick=\"front_call({method:\'detalle_ctas\',title:\'Cuotas de servicios Pagas en fecha\',elem:'srv',e_index:"+i+",action:\'ctas_pagas\'});\"><li class=\"list-group-item d-flex justify-content-between align-items-center\">Cuotas Pagas<span class=\"badge badge-success badge-pill\">"+this._data.srv[i]['ctas_pagas'].events.length+"</span></li></a>";
			r += "<a href=\'#\' onClick=\"front_call({method:\'detalle_ctas\',title:\'Cuotas de servicios Adelantadas\',elem:'srv',e_index:"+i+",action:\'ctas_adelantadas\'});\"><li class=\"list-group-item d-flex justify-content-between align-items-center\">Cuotas Adelantadas<span class=\"badge badge-success badge-pill\">"+this._data.srv[i]['ctas_adelantadas'].events.length+"</span></li></a>";
			r += "<a href=\'#\' onClick=\"front_call({method:\'detalle_ctas\',title:\'Cuotas de servicio Restantes\',elem:'srv',e_index:"+i+",action:\'ctas_restantes\'});\"><li class=\"list-group-item d-flex justify-content-between align-items-center\">Cuotas Restantes<span class=\"badge badge-success badge-pill\">"+this._data.srv[i]['ctas_restantes'].events.length+"</span></li></a>"
			r += "<a href=\'#\' onClick=\"front_call({method:\'detalle_ctas\',title:\'Cuotas de servicios Pagas fuera de termino\',elem:'srv',e_index:"+i+",action:\'ctas_pft\'});\"><li class=\"list-group-item d-flex justify-content-between align-items-center\">Cuotas Pagas Fuera de Termino<span class=\"badge badge-success badge-pill\">"+this._data.srv[i]['ctas_pft'].events.length+"</span></li></a>";

			r += "<a href=\'#\' onClick=\"front_call({method:\'detalle_ctas\',title:\'Cuotas de servicios Restantes\',elem:'srv',e_index:"+i+",action:\'ctas_mora\'});\"><li class=\"list-group-item d-flex justify-content-between align-items-center\">Cuotas en mora<span class=\"badge badge-danger badge-pill\">"+this._data.srv[i]['ctas_mora'].events.length+"</span></li></a>"

			r += "</ul></div></div></div><hr/>";
			// *** FINAL SERVICIOS

		}
		return r;
	},

	kill_service_btn : function(srv){
		if(parseInt(srv.tot_pagado) === 0 && TOP.permisos <= 5 ){
			return "<button type=\"button\" class=\"btn-normal mr-2\" onclick=\"front_call({method:'kill_service_elem',sending:false,elm_id:'"+srv.srvc_id+"'})\"><i class=\"material-icons \">remove-circle-outline</i</button></li>";
		}else{
			return '';
		}
	},

	//  *****************************************

	// TEXTO DE COMENTARIOS Y OBSERVACIONES
	ventana_de_mensages : function(){
			let r ="<div class=\'col d-flex flex-wrap p-2 \' id=\'txt_obsrv\' >";
			r += data_box_small.create({id:'txt_obsrv',label:"Mensages",value:this._data.lote.observaciones,pcle_lbl:'observaciones',edit_btn:true,collapsed:true}).get_screen();
			r += "</div>";
			return r;
	},


	//  *****************************************

	// ************** CARD 1 OK *****************

	get_card1: function(){
		let r ='';
		//*** ROW STATE AND FEC INIT
		if(TOP.permisos <= 10){
			// r+="<div class=\'row d-flex justify-content-around mt-5 p-2\'>"+this.get_curr_state()+this.get_fec_init()+"</div>";
		}
		// es cliente web
		if(TOP.permisos >= 10){
			// r+="<div class=\'row d-flex justify-content-between mb-1 p-2\'><div class='col'><img src='/images/logo_LPT1.svg'></div><div class='col text-right'><a href='https://lotesparatodos.com.ar'>Hola "+this._data.lote.cli_data[0]['value']+"<br/>cerrar sesión</a></p></div></div>";
		}

		//*** LOTES CARD
		r += "<div class=\'jp-card\' id='card1'>";
		// *** HEADING
		r += "<div id=\"heading_card1\" class='card-header d-flex justify-content-between'>"
		// r += "<button type=\"button\" onClick=front_call({'method':'back'}) class=\"btn-normal \"><i class=\"material-icons \">home</i></button>";
		// r +="<button type=\"button\" class=\"btn-normal  \"  data-toggle=\"collapse\" data-target=\"#collapse_card1\" aria-expanded=\"true\" aria-controls=\"card1_body\"><i class=\"material-icons \">more_vert</i></button>";
		r += this.get_header();
		if(TOP.permisos <= 10){
			// r += "<button type=\"button\" class=\"btn-normal\" onClick=front_call({method:'call_edit',action:'call',sending:true,data:{type:'Atom',id:"+this._data.lote['cli_id']+"}})><i class=\"material-icons \">open_in_new</i><span class=\'align-top \'>Editar Datos</span></button>";
		}
		r += "</div>" // CIERRA EL HEADING;
		r +="<div id='collapse_card1' class='collapse show' aria-labelledby='heading_card1' style=''>"
		//*** card 1
		r += "<div class=\'card-body\' id='card1_body' >";
		// r += this.lote_card();
		// r += this.service_cards();
		// r += "<div class=\'row justify-content-between mb-3\'>";
		// r += "<div class=\'title-lote d-flex\'>";
		// r += "<img class=\'jp-icon\' src=\'images/icons/home_blue_small.png\'></img> ESTADO DE CUENTA DE "+this._data.cli_nombre;
		// r += "</div>"
		// r += "<div class=\'title-lote d-flex\'>";
		// r += "<a href=\'https://sgt.escobar.gov.ar/pagosDeudaOnline/servlet/com.pagosdeudaescobar.wpdeudaonline\' target=\'_blank\'><button type=\"button\" class=\"btn-normal\" >DEUDA ONLINE</button></a>"
		// r += "</div>"
		// r += "<div class=\'title-lote d-flex\' >PARTIDA: ";
		// r += "<input type=\'text\'  readonly style=\'width:130px;border:0;\' value=\'"+this._data.lote['partida']+"\' id=\'nro_partida\' />";
		// r += "<a href=\'#\' onClick='copy_to_clipboard()'><i class=\"material-icons \" title='Copiar al Portapapeles' >file_copy</i></a>"
		// r += "</div></div>" // CIERRO EL ROW

		r += this.ventas_list();
		r += "<hr />";
		r += "<div class=\'row justify-content-center\'><div class=\'title-lote d-flex\ mr-5'>";
		r +="Total Vendido : "+accounting.formatMoney(parseInt(this._data.total_vendido), "$ ", 0, ".", ",")+"</div>";
		r +="<div class=\'title-lote d-flex mr-5 \'>Total Cobrado : "+accounting.formatMoney(parseInt(this._data.total_cobrado), "$ ", 0, ".", ",")+"</div>";
		r +="<div class=\'title-lote d-flex\'>Saldo a Cobrar : "+accounting.formatMoney(parseInt(this._data.saldo_a_cobrar), "$ ", 0, ".", ",")+"</div>";
		r += "</div>"


		//****  SERVICIOS
		// if(this._data.srv.length > 0){
		// 	// SI HAY SERVICIOS CON CUOTAS RESTANTES O CTA UPC LO PONE EN PANTALLA
		// 	if(this.verify_servicio_pendiente()){
		// 		r += "<div class='title-servicios'><img class=\'jp-icon\' src=\'images/icons/servicios_small.png\'></img> ESTADO DE CUENTA DE"+(TOP.permisos >= 10 ?" TUS ":" ")+"SERVICIOS</div>";
		// 		// TOSDOS LOS SERVICIOS QUE NO SON PRESTAMO
		// 		r += this.ctas_srv()
		// 		// TODOS LOS PRESTAMOS
		//
		// 		// for (let c = 0 ; c < t.length ; c ++){
		// 			// if(this._data.srv[i].srvc_name.indexOf('Prestamo') == 0 ){
		// 				// ****  PRESTAMOS
		// 				r += this.ctas_prest()
		// 			// }
		// 		// }
		// 	}
		// }
		r += "<hr />";
		//***  ROW DE BUTTONS
		r +=this.get_buttons_bar();

		//** ROW DE VENTANAS DE MENSAGE Y ARCHIVOS UPLOADED
		r +="<div class='row d-flex justify-content-around mt-2'>"
		// r += "<div class=\'row d-flex justify-content-between\'><div class='col-3'>";
		// if(TOP.permisos < 10){
		// 	r += this.ventana_de_mensages();
		// }

		// if(TOP.permisos > 10){
		// 	r += uploaded_files_boxes.create(['web_cli']).get_screen();
		//
		// }else{
		// 	r += uploaded_files_boxes.create(['web_cli','lote_data_gen']).get_screen();
		// }
		r +="</div>" //*** CIERRA ROW DE VENTANAS MSG Y  UPLOADED
		r += "</div></div></div></div>" //** CIERRA CARD BODY Y CARD1;
		return r;
	},
	// **********************************************

	verify_servicio_pendiente : function(){
		for (let i = 0 ; i < this._data.srv.length ; i ++){
			if(this._data.srv[i].cta_upc.total > 0 || this._data.srv[i].ctas_restantes.total > 0 ){
				return true;
			}
		}
		return false;
	},

	// **************  CARD DE RESCINDIDO  *****************
	get_card_rscn : function(){
		let r = "<div class=\'jp-card\'><div class='card-header  d-flex justify-content-center'>";
		r += "<h5 class=\'card-title\' >Rescisión de Contrato </h5></div>";
		r += "<div class=\'card-body\'>"+otbl.create([this._data.lote.rscn_data],'table_rscn')+"</div>";
		r +="</div>";
		console.log('resci',r);
		return r;
	},


	// **************  ULTIMOS MOVIMIENTOS  *****************

	get_card2 : function(){
		let r= "<div class='jp-card' id='card1' ><div class='card-header  d-flex justify-content-start' id=\"heading_card2\" >";
		r +="<button type=\"button\" class=\"btn-normal  \"  data-toggle=\"collapse\" data-target=\"#collapse_card2\" aria-expanded=\"true\" aria-controls=\"card2_body\"><i class=\"material-icons \">more_vert</i></button>";
		r += "<h5 class=\'card-title\'>Últimos Movimientos</h5></div>";
		r +="<div id='collapse_card2' class='collapse show' aria-labelledby='heading_card2' style=''>"
		r += "<div class=\'card-body\' id='card2_body'>"+otbl.create(this._data.last_mov,'table_last_movs')+"</div>";
		r +="</div></div>";
		return r;
	} ,
	// *********************************************


	// **************  SERVICIOS CANCELADOS  *****************

	get_card_canclds : function(){
		let t_data = new Array();
		for (var i = 0 ; i < this._data.srv.length ; i ++){
			if(this._data.srv[i].ctas_restantes.events.length === 0 && this._data.srv[i].ctas_mora.events.length === 0){

				t_data.push({
					'Plan / Servicio':(this._data.srv[i].srvc_name).substring(0,25),
					'Ctas. Pagas':this._data.srv[i].ctas_pagas.events.length,
					'Monto':this.get_tot_pagado(this._data.srv[i]),
					});
			}

		}
		if(t_data.length >0){
			let r= "<div class='jp-card' id='card3'><div class='card-header  d-flex justify-content-start' id=\"heading_card3\"  >";
			r +="<button type=\"button\" class=\"btn-normal  \"  data-toggle=\"collapse\" data-target=\"#collapse_card3\" aria-expanded=\"true\" aria-controls=\"card3_body\"><i class=\"material-icons \">more_vert</i></button>";
			r += "<h5 class=\'card-title\' >Planes Servicios Cancelados </h5></div>";
			r +="<div id='collapse_card3' class='collapse show' aria-labelledby='heading_card3' style=''>"
			r += "<div class=\'card-body\' id='card3_body' >"+otbl.create(t_data,'table_cancs')+"</div>";
			r +="</div></div>";
			return r;
		}
	} ,
	// *********************************************


	// ************** Printed version *****************
	get_print_vers: function(){
		//*** ROW STATE AND FEC INIT
		let r ="<div class='container p-3'><div class=\'row d-flex justify-content-center m-2\'></div>";
		//*** HEADER
		r += "<div class=\'card bg-light mb-2 \'><div class='card-header d-flex justify-content-between'>";
		r += "<div class='col-8 d-flex justify-content-start'><h5> Nombre de Cliente: "+this._data.lote.cli_atom_name+"<br/>"
		r += "Numero de Lote: "+this._data.lote.lote_nom+"<br/>"
		r += "Plan de Financiación: "+this._data.lote.financ+"<br/>";
		r += "Fecha de Inicio del plan:  "+this._data.lote.fec_init + "</h5></div>"
		r +="<div class='col-3 d-flex justify-content-end'><img src=\"/images/logo_LPT1.svg\"></div>"
		r += "</div>"
		//*** LOTE Y SERVICIOS
		r += "<div class=\'card-body\'>";
		// r += this.lote_card();
		// r += this.service_cards();
		r += this.ctas_tbl_noacc();
		r += "<hr/><div class=\'row d-flex justify-content-start\'><h5 class=\'p-2\'>Credito Disponible: "+accounting.formatMoney(parseInt(this._data.lote.mto_reintegro), "$ ", 0, ".", ",")+"</h5></div>";
		r += "</div></div></div></div>";
		return r;
	},
	// **********************************************

};
