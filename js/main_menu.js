// CREA EL MENU
var menu = {
  create:function(v){
    //***  ROW & COL CENTRAL
    let f = "<div class=\'row mt-4 d-flex justify-content-center\'>";
    //  LOGO
    f += "<div class='col-xl-4 col-md-6 col-sm-8 text-center\'><img width='110px' src=\'images/nuberio-iso.png\'/>"
    f +="<div class=\'card m-3\'>"
    f +="<div class=\'card-body\'>";
    f +="<div class=\'accordion\' id=\'accordion_cont\'>"
    f += Object.keys(v).map(x=>{
      let t = "";
      t += menu_items.create(v[x]);
      return t;
    }).join('');
    f +="</div></div></div></div></div></div>";
    return f;
  }
}

// CREA UN BOX CON LOS LINKS DE FILTRADO
var menu_items = {
	create:function(v){
    t = ''
    // TITLE
    t +="<div class=\'card \'>"
    t +="<div class=\'card-header \'>";
		t +="<div class=\'d-flex collapsed\' data-toggle=\'collapse\' data-target=\'#lg_"+v[0].controller
    t +="\' aria-expanded=\'false\' aria-controls=\'lg_"+v[0].controller+"\' >"
    t += v[0].nombre
    t +="</div>";
		t +="</div>";
		// CONTENIDO DE COL CON SUB ITEMS
		t +="<div class=\'collapse\' id=\'lg_"+v[0].controller+"\' data-parent=\"#accordion_cont\" >";
    if(v[1].length >0){
      // LINK DE METHOD SUB ITEMS
      t += v[1].map(i=>{return "<li class=\'list-group-item d-flex justify-content-start\'><div class='btn btn-style-6 btn-primary btn-sm' onClick=\"menu_control(\'"+v[0].controller+"\',\'"+(i.method?i.method:i.method)+"\')\">"+ i.nombre +"</div></li>" }).join('');
    }
		t += "</div></ul>"
		return t;
	}
}

// *********************************************
// 6-mayo  2020
// main-menu
// *********************************************
function menu_control(controller,method){
	TOP.current_page ++;
	front_call({
		method:method,
		controller:controller,
		target:TOP.current_page,
		sending:true
	});
	$('#slider').carousel('next')
}

// *********************************************
// 6-mayo  2020
// main  back button
// *********************************************
function bback(page_num){
	$('#slider').carousel('prev');
	$('#container_'+TOP.current_page).html('');
  $('#navbar_msg_'+TOP.current_page).html('');
  TOP.current_page --;
}
